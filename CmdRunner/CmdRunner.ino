#include <Servo.h>
//#include <SoftwareSerial.h>

#define KEYSIZE 115
#define BAUD 9600
#define CHUNKSIZE 7
#define HAND 0
#define FIRSTFINGER 2
#define LASTFINGER 6
#define THRESHOLD 0
#define FINGERSNUM 5
#define HANDSTEP 12
#define HANDDIR 8
#define HANDENABLE 4
#define QUEUESIZE 500
/*
#define UP_ANGLE5 80
#define DOWN_ANGLE5 130
#define UP_ANGLE4 0
#define DOWN_ANGLE4 40
#define UP_ANGLE3 60
#define DOWN_ANGLE3 130
#define UP_ANGLE2 40
#define DOWN_ANGLE2 100
#define UP_ANGLE1 100
#define DOWN_ANGLE1 160
*/
#define UP_ANGLE5 80
#define DOWN_ANGLE5 130
#define UP_ANGLE4 0
#define DOWN_ANGLE4 40
#define UP_ANGLE3 30
#define DOWN_ANGLE3 180
#define UP_ANGLE2 20
  #define DOWN_ANGLE2 130
#define UP_ANGLE1 0
#define DOWN_ANGLE1 180

long up_angle[FINGERSNUM] = {UP_ANGLE1, UP_ANGLE2, UP_ANGLE3, UP_ANGLE4,UP_ANGLE5};
long down_angle[FINGERSNUM] = {DOWN_ANGLE1, DOWN_ANGLE2, DOWN_ANGLE3, DOWN_ANGLE4, DOWN_ANGLE5};

#define RX 2
#define TX 8

#define ABS(x) ((x) > 0) ? (x) : ((-1)*(x))

//SoftwareSerial Serial1(RX, TX); // RX, TX
struct ArduinoInputFormat
{
  unsigned long startTimeMillis;
  byte motorId;
  signed char steps;
  byte velocity;
};
int pos = 0;
unsigned long StartTime;
Servo finger[FINGERSNUM];


void RunStepper(int steps) {
  for (int x = 0; x < KEYSIZE * steps; x++) {
    digitalWrite(HANDSTEP, HIGH);
    delayMicroseconds(500);
    digitalWrite(HANDSTEP, LOW);
    delayMicroseconds(500);
  }
  Serial.print("hand move ");
  Serial.print(steps);
  Serial.println(" steps");
  return;
}
void ServoDown(int motorId) {
  finger[motorId - 2].write(down_angle[motorId - 2]);

  Serial.print("finger id = ");
  Serial.print(motorId);
  Serial.println(" down");
  return;
}
void ServoUp(int motorId) {
  finger[motorId - 2].write(up_angle[motorId - 2]);

  Serial.print("finger id = ");
  Serial.print(motorId);
  Serial.println(" up");
  return;
}

void ServoDownSync(int motorId) {
  for (pos = UP_ANGLE1; pos < DOWN_ANGLE1; pos++) {
    finger[motorId - 2].write(pos);
  }
  Serial.print("finger id = ");
  Serial.print(motorId);
  Serial.println(" up");
  return;
}
void ServoUpSync(int motorId) {
  for (pos = DOWN_ANGLE1; pos > UP_ANGLE1; pos--) {
    finger[motorId - 2].write(pos);
  }
  Serial.print("finger id = ");
  Serial.print(motorId);
  Serial.println(" down");
  return;
}

unsigned long last = 0;
unsigned long first = 0;
ArduinoInputFormat cmdQueue[QUEUESIZE];
ArduinoInputFormat currCmd;
long queue_size = 0;
long getQueueSize() {
  return queue_size;
}
int enqueue(ArduinoInputFormat cmd) {
  if (getQueueSize() == QUEUESIZE) return -1;
  cmdQueue[last++] = cmd;
  last = last % QUEUESIZE;
  queue_size++;
  return 0;
}
int dequeue(ArduinoInputFormat *cmd) {
  if (getQueueSize() == 0) return -1;
  *cmd = cmdQueue[first++];
  first = first % QUEUESIZE;
  queue_size--;
  return 0;
}

#define GETSIZE 2
#define GETCHUNK 3
unsigned long chunkCount = 0;
struct wifiCmd {
  byte cmd;
  unsigned long payload;
};

int sendWifiCmd(byte cmd, unsigned long payload) {
  Serial.println("Wifi cmd call");
  wifiCmd wificmd = {cmd, payload};
  char b[sizeof(wifiCmd)];
  int i = 0;
  memcpy(b, &wificmd, sizeof(wifiCmd));
  for (i = 0; i < sizeof(wifiCmd); i++) {
    Serial1.write(b[i]);
    Serial.print((int)b[i]);
  }
  return 0;
}

unsigned long SongSize = 0;
unsigned long NumReadCommandsFromWifi = 0;
unsigned long ByteCounter = 0;
int readArduinoCmd(ArduinoInputFormat *cmd) {
  if (!NumReadCommandsFromWifi) return -1;
  if (!ByteCounter && !sendWifiCmd(GETCHUNK, chunkCount++)) {
    ByteCounter = CHUNKSIZE;
    Serial.print("send get chunk, chunknum");
    Serial.println(chunkCount - 1);
  }
  char b[sizeof(ArduinoInputFormat)];
  Serial.print(">>> ByteCounter =");
  Serial.println(ByteCounter);
  int i = 0;
  for (i = 0; i < sizeof(ArduinoInputFormat); i++) {
    //  if (!Serial1.available()) return -1;
    Serial.print(">>>>>>>>read byte:");
    if (ByteCounter <= 0) return -1;
    Serial.print(">>>>>>>>read byte:");
    while (!Serial1.available());
    b[i] = Serial1.read();
    ByteCounter--;
    Serial.println((int)b[i]);
  }
  memcpy(cmd, b, sizeof(ArduinoInputFormat));
  NumReadCommandsFromWifi--;
  return 0;
}

unsigned long getSongSize() {
  if (SongSize > 0) return SongSize;
  Serial.println("start song");
  /*                                  return 10;*/

  sendWifiCmd(GETSIZE, 0);
  delay(5000);
  unsigned long Size;
  char b[sizeof(unsigned long)];
  int i = 0;
  for (i = 0; i < sizeof(unsigned long); i++) {
    while (!Serial1.available()) delay(50);
    b[i] = (char) Serial1.read();
    Serial.println("read 1 byte");
  }
  memcpy(&Size, b, sizeof(unsigned long));
  Serial.print("get size work print Size = ");
  Serial.println(Size);
  return Size / sizeof(ArduinoInputFormat);
}

void fillCommands() {
  if (!SongSize || getQueueSize() == QUEUESIZE || NumReadCommandsFromWifi == 0) return;
  /*                   ArduinoInputFormat tmp = {70,1,70,0};
                     ArduinoInputFormat tmp2 = {90,2,70,0};
                     ArduinoInputFormat tmp3 = {110,2,-70,0};
                     ArduinoInputFormat tmp4 = {150,1,-70,0};
                     ArduinoInputFormat tmp5 = {170,2,70,0};
    enqueue(tmp);
    enqueue(tmp2);
    enqueue(tmp3);
    enqueue(tmp4);
    enqueue(tmp5);
    return;*/
  ArduinoInputFormat cmd;
  if (readArduinoCmd(&cmd)) return;
  enqueue(cmd);
  return;
}


void startSong() {
  digitalWrite(4, HIGH); // Set Enable low
  chunkCount = 0;
  SongSize =  getSongSize();
  NumReadCommandsFromWifi = SongSize;
  delay(2000);
  while (getQueueSize() < QUEUESIZE && NumReadCommandsFromWifi > 0) {
    fillCommands();
    delay(50);
  }
  StartTime = millis();
  return;
}
int consume_cmd = 0;
int command_count = 0;
void runCommand() {
  Serial.println("-----------RUN COMMAND----------");
  /* Serial.print(millis());
    Serial.print("!=");
    Serial.print(currCmd.startTimeMillis);
    Serial.print(">>>");
    Serial.println(getQueueSize());*/
  if (getQueueSize() == 0 && !consume_cmd) return;
  if (!consume_cmd) {
    dequeue(&currCmd);
    consume_cmd = 1;
  }
  if (consume_cmd && StartTime + currCmd.startTimeMillis <= millis()) {
    if (StartTime + currCmd.startTimeMillis < millis()) StartTime += millis() - (currCmd.startTimeMillis + StartTime);
    Serial.println("-------TIME TO RUN COMMAND----------");
    switch (currCmd.motorId) { // if motorid!=HAND or FINGERS so it's the end of the song
      case HAND:
        Serial.println("-----------HAND COMMAND----------");
        digitalWrite(4, LOW); // Set Enable low
        if (!(currCmd.steps > 0)) {
          digitalWrite(HANDDIR, HIGH);
        } else {
          digitalWrite(HANDDIR, LOW);
        }
        RunStepper(ABS(currCmd.steps));
        break;
      case FIRSTFINGER...LASTFINGER:
        Serial.println("-->>>>>----   --finger COMMAND----------");
          digitalWrite(4, HIGH); // Set Enable high
        if (currCmd.steps > 0) {
          ServoDown(currCmd.motorId);
        } else {
          ServoUp(currCmd.motorId);
        }
        break;
      default:
        break;
    }
    SongSize--;
    consume_cmd = 0;
    command_count++;
    Serial.print("===========>>===========>>");
    Serial.print(getQueueSize());
    Serial.print("===========>>===========>>  ");
    Serial.println(command_count);

  }
  return;
}

void setup() {
    // Stepper
  //delay(1);
  pinMode(4, OUTPUT); // Enable
  digitalWrite(4, HIGH); // Set Enable low

  pinMode(HANDSTEP, OUTPUT);
  pinMode(HANDDIR, OUTPUT);


  Serial.begin(BAUD);
  Serial1.begin(9600);
  Serial.println("end");


  // Servo
  for (int i = 0; i < FINGERSNUM; i++) {
    finger[i].attach(i + 30);
    ServoUp(i+2);
  }
 delay(15000);
 while (Serial1.read() != -1);
 //digitalWrite(4, LOW); // Set Enable low
}

unsigned long j = 0;
void loop() {
  // put your main code here, to run repeatedly:
  /*if(j==0){
    Ser.write(2);
    Ser.write(0);
    Ser.write(0);
    Ser.write(0);
    Ser.write(0);
    Serial.print("writing");
    Serial.println(j);
    j+=1;
    }
    if(Ser.available()>0)
    Serial.print((int)Ser.read());*/
  //Serial.println("read 1 byte");
  startSong();
  while (SongSize > 0 || getQueueSize() > 0) {
    // Serial.println("SongSize > 0 or getQueueSize > 0");
    fillCommands();
    runCommand();
  }
}

