﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BotHoven
{

    struct ArduinoInputFormat {
        public int startTimeMillis;
        public int motorId;
        public int steps;
        public int velocity;
    }

    class Settings
    {

        public int handsNum;
        public bool[][] fingerConf; /* true == white, false == black */
        public int stepsPerKey;
        public int[] keysTransitionMs;
        public int[] fingerDownMs;
        public int[] fingerUpMs;
        public int[] fingerNums;
        public bool[] isKeyWhiteBool;
        public int firstKey;
        public int lastKey;
        public int[] handsMotorIds;
        public int[] fingersMotorIds;
        public double noteTolerancePrecentage;

        string getKeyValue(string key)
        {
            return ConfigurationManager.AppSettings[key].Replace(" ", "");
        }

        int[] keyToArrInt(string key)
        {
            int[] intVals;
            string[] strVals = getKeyValue(key).Split(',');
            intVals = new int[strVals.Length];
            for (int i = 0; i < strVals.Length; i++)
            {
                intVals[i] = int.Parse(strVals[i]);
            }

            return intVals;
        }

        public bool isKeyWhite(int keyNum)
        {
            return isKeyWhiteBool[keyNum];
        }

        public Settings()
        {
            
            int[] blackKeysIdxs = {22, 25, 27, 30, 32, 34, 37, 39,
                                   42, 44, 46, 49, 51, 54, 56, 58,
                                   61, 63, 66, 68, 70, 73, 75, 78,
                                   80, 82, 85, 87, 90, 92, 94, 97,
                                   99, 102, 104, 106};
            isKeyWhiteBool = Enumerable.Repeat(true, 150).ToArray();
            foreach (int key in blackKeysIdxs) {
                isKeyWhiteBool[key] = false;
            }

            handsNum = int.Parse(getKeyValue("handsNum"));
            fingerConf = new bool[handsNum][];
            fingerNums = new int[handsNum];
            int i = 0;

            foreach (string handConf in getKeyValue("fingerConf").Split(','))
            {
                fingerConf[i] = new bool[handConf.Length];
                fingerNums[i] = 0;
                for (int j = 0; j < handConf.Length; j++)
                {
                    fingerConf[i][j] = (handConf[j] == '0');
                    fingerNums[i]++;
                }
                ++i;
            }

            stepsPerKey = int.Parse(getKeyValue("stepsPerKey"));

            keysTransitionMs = keyToArrInt("keysTransitionMs");
            fingerDownMs = keyToArrInt("fingerDownMs");
            fingerUpMs = keyToArrInt("fingerUpMs");
            firstKey = int.Parse(getKeyValue("firstKey"));
            lastKey = int.Parse(getKeyValue("lastKey"));
            handsMotorIds = keyToArrInt("handsMotorIds");
            fingersMotorIds = keyToArrInt("fingersMotorIds");
            noteTolerancePrecentage = (100 - Double.Parse(getKeyValue("noteTolerancePrecentage"))) / 100;

        }

    }


    class Note
    {
        public int key;
        public int startTime;
        public int duration;
        public int velocity;
        public int finger;

        public Note (int key, int startTime, int duration, int velocity)
        {
            this.key = key;
            this.startTime = startTime;
            this.duration = duration;
            this.velocity = velocity;
        }

    }
	
	

    class Program
    {

        static Settings set = new Settings();
        static List<Note> inputNotes_G = new List<Note>();
        static List<ArduinoInputFormat> ardCommands = new List<ArduinoInputFormat>();
        static int notesNum = 0;


        enum CmdLineArgs 
        {
            MidiFilePath = 0,
            // Must be last
            Count
        };

        static void readInputFile(string filename)
        {
            string line;
            string[] args;

            System.IO.StreamReader file =
                new System.IO.StreamReader(filename);

            while ((line = file.ReadLine()) != null)
            {
                args = line.Split(',');
                inputNotes_G.Add(new Note(int.Parse(args[0]),
                                          int.Parse(args[1]),
                                          int.Parse(args[2]),
                                          int.Parse(args[3])
                                         )
                                );
                ++notesNum;
            }

            file.Close();
        }

        static int getLeftFingerKey(int hand, int key, int finger)
        {
            int pos = key;
            for (int i = finger; i > 0; i--)
            {
                if (set.isKeyWhite(pos) == set.fingerConf[hand][i])
                {
                    pos--;
                }
            }

            return pos;
        }

        static int getDistInKeys(int hand, int srcKey, int srcFin, int dstKey, int dstFin)
        {
            int leftFingerSrc = getLeftFingerKey(hand, srcKey, srcFin);
            int leftFingerDest = getLeftFingerKey(hand, dstKey, dstFin);
            int dist = 0;

            for (int i = Math.Min(leftFingerSrc,leftFingerDest);
                 i < Math.Max(leftFingerSrc, leftFingerDest);
                 i++)
            {
                if (set.isKeyWhite(i))
                {
                    dist++;
                }
            }

            if (leftFingerSrc > leftFingerDest) {
                dist *= -1;
            }

            return dist;
        }

        static int getTransitionTimeMs(int srcKey, int srcFin, int dstKey, int dstFin)
        {
            int dist = getDistInKeys(0, srcKey, srcFin, dstKey, dstFin);
            return set.fingerUpMs[srcFin] + set.keysTransitionMs[dist] + set.fingerDownMs[dstFin];
        }

        static double getSpeedUpFactor(int keyIndex, int srcFin, int dstFin)
        {
            int origDuration = inputNotes_G[keyIndex].duration;
            int dist = Math.Abs(getDistInKeys(0, inputNotes_G[keyIndex - 1].key, srcFin, 
                                              inputNotes_G[keyIndex].key, dstFin));
            if (dist == 0 && srcFin != dstFin)
            {
                return 100;
            }

            int feasableTime = set.fingerUpMs[srcFin] + 
                               set.keysTransitionMs[dist] + 
                               set.fingerDownMs[dstFin];

            return ((double)origDuration * set.noteTolerancePrecentage / (double)feasableTime);
        }

        static double calcMoveTimes()
        {
            double[][] dynamicFactorsTable = new double[notesNum][];
            int[][] dynamicTableChoice = new int[notesNum][];

            dynamicFactorsTable[0] = new double[set.fingerNums[0]];
            dynamicTableChoice[0] = new int[set.fingerNums[0]];

            for (int fin = 0; fin < set.fingerNums[0]; fin++)
            {
                dynamicFactorsTable[0][fin] = 1;
                dynamicTableChoice[0][fin] = 0;
            }

            for (int note = 1; note < notesNum; note++)
            {
                dynamicFactorsTable[note] = new double[set.fingerNums[0]];
                dynamicTableChoice[note] = new int[set.fingerNums[0]];
                for (int fin = 0; fin < set.fingerNums[0]; fin++)
                {
                    /* If this finger cannot play this note (different colors) */
                    if (set.isKeyWhiteBool[inputNotes_G[note].key] != set.fingerConf[0][fin])
                    {
                        dynamicFactorsTable[note][fin] = -1;
                        dynamicTableChoice[note][fin] = -1;
                        continue;
                    }

                    dynamicFactorsTable[note][fin] = getSpeedUpFactor(note, 0, fin);
                    dynamicTableChoice[note][fin] = 0;
                    for (int srcFin = 0; srcFin < set.fingerNums[0]; srcFin++)
                    {
                        if (getSpeedUpFactor(note, srcFin, fin) > dynamicFactorsTable[note][fin] &&
                            dynamicTableChoice[note - 1][srcFin] != -1)
                        {
                            dynamicFactorsTable[note][fin] = Math.Min(getSpeedUpFactor(note, srcFin, fin),
                                                                      dynamicFactorsTable[note - 1][srcFin]);
                            dynamicTableChoice[note][fin] = srcFin;
                        }
                    }
                }
            }

            double maxSpeedUp = 0;

            inputNotes_G[notesNum - 1].finger = 0;

            for (int fin = 0; fin < set.fingerNums[0]; fin++)
            {
                if (dynamicFactorsTable[notesNum - 1][fin] > maxSpeedUp)
                {
                    maxSpeedUp = dynamicFactorsTable[notesNum - 1][fin];
                    inputNotes_G[notesNum - 1].finger = fin;
                }
            }

            for (int note = notesNum - 2; note >= 0; note--)
            {
                inputNotes_G[note].finger = dynamicTableChoice[note + 1][inputNotes_G[note + 1].finger];
            }

            return Math.Min(maxSpeedUp, 1);

        }

        static void applySpeedUpFactor(double speedUpFactor)
        {
            for (int i = 0; i < notesNum; i++)
            {
                inputNotes_G[i].startTime = (int)(inputNotes_G[i].startTime / speedUpFactor);
                inputNotes_G[i].duration = (int)(inputNotes_G[i].duration / speedUpFactor);
            }
        }

        static void fillArduinoCommands()
        {

            ArduinoInputFormat com;
            int dist;
            int offset = 0;

            for (int i = 0; i < notesNum; i++)
            {
                /* Hand movement command */
                if (i == 0)
                {
                    dist = getDistInKeys(0, set.firstKey, 0,
                                         inputNotes_G[i].key, inputNotes_G[i].finger);
                }
                else
                {
                    dist = getDistInKeys(0, inputNotes_G[i - 1].key, inputNotes_G[i - 1].finger,
                                         inputNotes_G[i].key, inputNotes_G[i].finger);
                }

                if (i == 0)
                {
                    offset = inputNotes_G[i].startTime - set.keysTransitionMs[Math.Abs(dist)]
                                          - set.fingerDownMs[inputNotes_G[i].finger];
                }

                if (dist != 0) {
                    com.steps = dist * set.stepsPerKey;
                    com.startTimeMillis = inputNotes_G[i].startTime - set.keysTransitionMs[Math.Abs(dist)]
                                          - set.fingerDownMs[inputNotes_G[i].finger] - offset;
                    com.motorId = set.handsMotorIds[0];
                    com.velocity = 0;
                    ardCommands.Add(com);
                }
                /* Finger down command */
                com.steps = 1;
                com.startTimeMillis = inputNotes_G[i].startTime - 
                                      set.fingerDownMs[inputNotes_G[i].finger] - offset;
                com.motorId = set.fingersMotorIds[inputNotes_G[i].finger];
                com.velocity = inputNotes_G[i].velocity;
                ardCommands.Add(com);

                /* Finger up command */
                com.steps = -1;
                com.startTimeMillis = inputNotes_G[i].startTime + inputNotes_G[i].duration - offset;
                com.motorId = set.fingersMotorIds[inputNotes_G[i].finger];
                com.velocity = 0;
                ardCommands.Add(com);

            }

            ardCommands.Sort(
                delegate (ArduinoInputFormat a, ArduinoInputFormat b) {
                    return a.startTimeMillis.CompareTo(b.startTimeMillis);
                }
            );

        }

        static void printResults(string inputFileName)
        {
            string outFileName = inputFileName + ".alg";
            string outLogName = inputFileName + ".log";

            System.IO.FileStream outFile =
                new System.IO.FileStream(outFileName, System.IO.FileMode.Create);
            System.IO.StreamWriter logFile = new System.IO.StreamWriter(outLogName, false);

            System.IO.BinaryWriter binStream = new System.IO.BinaryWriter(outFile);

            foreach (ArduinoInputFormat line in ardCommands)
            {
                binStream.Write(line.startTimeMillis);
                binStream.Write(line.motorId);
                binStream.Write(line.steps);
                binStream.Write(line.velocity);
                logFile.WriteLine("{0:D} {1:D} {2:D} {3:D}", 
                    line.startTimeMillis, line.motorId, line.steps, line.velocity);

            }

            binStream.Close();
            outFile.Close();
            logFile.Close();
        }
        static void runTester()
        {
            Hand[] hands = new Hand[2];
            hands[0] = new Hand(new Piano(notesNum), set.fingerUpMs, set.fingerDownMs, set.keysTransitionMs);
            hands[1] = new Hand(new Piano(notesNum), set.fingerUpMs, set.fingerDownMs, set.keysTransitionMs);
            List<ArduinoInputFormat>.Enumerator e;
            int prevTime = 0, curTime = 0;
            for (e = ardCommands.GetEnumerator(); ; e.MoveNext())
            {
                ArduinoInputFormat Commands = e.Current;
                if (Commands.startTimeMillis != 0)
                {
                    curTime = Commands.startTimeMillis;
                    System.Threading.Thread.Sleep(System.Math.Abs(curTime - prevTime));
                }
                if (Commands.motorId == 0 || Commands.motorId == 1)
                {
                    hands[0].move(Commands.steps);
                }
                else
                {
                    if (Commands.steps > 0)
                    {
                        hands[0].downFinger(Commands.motorId - 2);
                    }
                    else
                    {
                        hands[0].upFinger(Commands.motorId - 2);
                    }
                }
                if (Commands.startTimeMillis != 0)
                {
                    prevTime = Commands.startTimeMillis;
                }
            }
        }   
        static void Main(string[] args)
        {
            if (args.Length < (int)CmdLineArgs.Count)
            {
                Console.Out.WriteLine("Invalid usage. Expected: BotHoven.exe <midi_parsed_file>");
                return;
            }
            string file = args[(int)CmdLineArgs.MidiFilePath];

            readInputFile(file);

            double speedUpFactor = calcMoveTimes();
            Console.Out.WriteLine("A speedup of {0:F} was acheived", speedUpFactor);
            applySpeedUpFactor(speedUpFactor);
            fillArduinoCommands();

            printResults(file);
            runTester();

            System.Threading.Thread.Sleep(5000);

        }
    }
}
