import sys
import mido
import exceptions

DEFAULT_INTERVAL = 0.3

def to_milli(secs):
	return int(secs * 1000)
	
def get_notes_times(voice_msgs):
	total_time = 0
	for msg in voice_msgs:
		total_time += msg.time
		yield total_time

def get_note_length(voice_msgs, msg_index):
	#time = voice_msgs[msg_index].time
	time = 0
	for i in xrange(msg_index + 1, len(voice_msgs)):
		time += voice_msgs[i].time
		if voice_msgs[i].note == voice_msgs[msg_index].note and voice_msgs[i].type == 'note_off':
			return time
		
	# default value
	raise exceptions.RuntimeError("WARNING: get_note_length: couldn't find note_off for msg = %r" % (voice_msgs[msg_index]))
	
def main(input_file_path, output_file):
	midi_file = mido.MidiFile(input_file_path)	
	voice_msgs = [msg for msg in midi_file if msg.type in ('note_on', 'note_off')]
	timestamps = list(get_notes_times(voice_msgs))
	
#	for msg in midi_file:
#		print msg
	
#	p = mido.open_output()
#	for msg in midi_file.play():
#		p.send(msg)
#		import pdb
#		pdb.set_trace()
#		print msg

	for msg_index in xrange(len(voice_msgs)):
		if voice_msgs[msg_index].note % 12 in [1, 3, 6, 8, 10]:
			print "Filtered voice_msgs[msg_index].note = %d" %(voice_msgs[msg_index].note,)
		elif voice_msgs[msg_index].type == 'note_on':
			output_file.write("%d,%d,%d,%d\n" %(voice_msgs[msg_index].note,
			to_milli(timestamps[msg_index]),
			to_milli(get_note_length(voice_msgs, msg_index)), 
			voice_msgs[msg_index].velocity))
	
if __name__ == "__main__":
	if len(sys.argv) < 3:
		print "%s <input_midi_file_path> <output_midi_file_path>" %(sys.argv[0],)
	else:
		main(sys.argv[1], file(sys.argv[2], "w"))