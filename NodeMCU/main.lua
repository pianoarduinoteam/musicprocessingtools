---------------------[[ Configuration ]]--------------------------
require "config"

---------------------[[ Constants ]]--------------------------
MAX_PAYLOAD_SIZE = 300 * 1024

STATE_INIT,
STATE_GOT_GET_TOTAL_SIZE,
STATE_RECEIVE_FROM_SERVER,
STATE_GOT_HEADER,
STATE_GOT_PAYLOAD,
STATE_GET_TOTAL_SIZE_RESPONDED,
STATE_GOT_GET_CHUNK
 = 0, 1, 2, 3, 4, 5, 6

GET_SONG_CONTENT_TIMER_ID = 0

COMMAND_GET_TOTAL_SIZE,
COMMAND_GET_CHUNK = 2, 3
---------------------[[ Globals ]]--------------------------
g_content_len = 0
g_state = STATE_INIT
 ---------------------[[ Utility Functions ]]--------------------------
 function base64Decode(data)
        local tab = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
        local a,b = data:gsub('=','A')
        local out = ''
        local l = 0
        for i=1,a:len() do
            l=l+tab:find(a:sub(i,i))-1
            if i%4==0 then 
                out=out..string.char(bit.rshift(l,16),bit.band(bit.rshift(l,8),255),bit.band(l,255))
                l=0
            end
            l=bit.lshift(l,6)
        end
        return out:sub(1,-b-1)
end
function base64DecodeFile(base64_payload_file_path, payload_file_path)
    file.remove(payload_file_path)
    decode_chunk_size = 1024 -- must be a multiple of 4
    chunks_count = math.ceil((getFileSize(base64_payload_file_path)) / decode_chunk_size)
    if chunks_count == 0 then
        --print("ERROR: zero chunks to decode")
        return
    end
    --print("TRACE: base64DecodeFile: ", chunks_count, " to file: ", payload_file_path)
    for i=0,chunks_count - 1 do
        chunk = readChunkFromFile(base64_payload_file_path, i, decode_chunk_size)
        while chunk:len() % 4 ~= 0 do
            chunk = chunk .. "="
        end
        decoded_chunk = base64Decode(chunk)
        --print("TRACE: appending: ", decoded_chunk:len(), " to file: ", payload_file_path)
        file.open(payload_file_path, "a")
        file.write(decoded_chunk)
        file.close()
    end
end
function numToBinaryString(x)
    local s = ""
    while x~=0 do
        s = s..string.char(x % 256)
        x = (x - x % 256) / 256
    end
    while s:len() < 4 do
        s = s..string.char(0)
    end
    return s
end
function binaryStringToNum(s)
    num = 0
    for i = 0,3 do
        num = num + string.byte(s, i+1)*256^i
    end
    return num
end
function getFileSize(path)
    file.open(path)
    size = file.seek("end", 0)
    file.close()
    return size
end
function readChunkFromFile(path, chunk_index, chunk_size)
    file.open(path)
    file_offset = chunk_index * chunk_size
    pos = file.seek("set", file_offset)
    if pos == nil then
        file.close()
       return nil
    end
    chunk = file.read(chunk_size)
    file.close()
    return chunk
end
function getFieldToken(payload, field_start, field_end)
    i, j = payload:find(field_start)
    if j ~= nil then
        length_start = j + 1
        i, length_end = payload:find(field_end, length_start)
        return payload:sub(length_start, length_end - 1)
    end
    return nil
end
function onUartReceive(data)
    --print("TRACE: onUartReceive is called.")
    if string.byte(data, 1) == COMMAND_GET_TOTAL_SIZE then
        -- Allow GET_TOTAL_SIZE override current chunks reads 
        if g_state > STATE_GOT_GET_TOTAL_SIZE and g_state < STATE_GET_TOTAL_SIZE_RESPONDED then
            --print("INFO: Already got GET_TOTAL_SIZE and it's handled. current_state: ", g_state)
            return
        end
        g_state = STATE_GOT_GET_TOTAL_SIZE
        getSongContent()
    elseif string.byte(data, 1) == COMMAND_GET_CHUNK then
        if (g_state < STATE_GET_TOTAL_SIZE_RESPONDED) then
            --print("ERROR: Haven't responded to GET_TOTAL_SIZE yet. current state: ", g_state)
            return
        end
        g_state = STATE_GOT_GET_CHUNK
        chunk_index_str = string.sub(data, 2, 5)
        chunk_index = binaryStringToNum(chunk_index_str)
        chunk = readChunkFromFile(PAYLOAD_FILE_PATH, chunk_index, ARDUINO_CHUNK_SIZE)
        if chunk ~= nil then
            uart.write(0, chunk)
        end
    else
        --print("ERROR: Got Invalid Command. Got: ", string.sub(data, 1, 1))
        uart.on("data")
        return 
    end
    
end
function onGetSongContentReceive(conn, payload)
    --print("TRACE: onGetSongContentReceive is called.")
    tmr.unregister(GET_SONG_CONTENT_TIMER_ID)
    if g_state == STATE_RECEIVE_FROM_SERVER then
        request_status = tonumber(getFieldToken(payload, "HTTP/1.1 ", " "))
        if request_status ~= 200 then
            --print("INFO: request_status ~! 200. Got: ", request_status)
            conn:close()
            tmr.alarm(GET_SONG_CONTENT_TIMER_ID, TIMER_TICK_INTERVAL_MS, tmr.ALARM_SINGLE, getSongContent)
            return
        end
        g_content_len = tonumber(getFieldToken(payload, "Length: ", "\r\n"))      
        if g_content_len == nil or g_content_len <= 2 then
            --print("Invalid Content-length. payload = ", payload)
            conn:close()
            tmr.alarm(GET_SONG_CONTENT_TIMER_ID, TIMER_TICK_INTERVAL_MS, tmr.ALARM_SINGLE, getSongContent)
            return
        end
        if g_content_len > MAX_PAYLOAD_SIZE then
            --print("INFO: Payload size too big. Got: ", g_content_len, "Max: ", MAX_PAYLOAD_SIZE)
            conn:close()
            tmr.alarm(GET_SONG_CONTENT_TIMER_ID, TIMER_TICK_INTERVAL_MS, tmr.ALARM_SINGLE, getSongContent)
            return
        end  
        
        -- Don't count wrapping "..."
        g_content_len = g_content_len - 2
        g_state = STATE_GOT_HEADER
        --print("INFO: Total content len: ", g_content_len, " bytes.")
        file.open(BASE64_PAYLOAD_FILE_PATH, "w")
        
        i, j = payload:find("\r\n\r\n")
        response_body_encoded = payload:sub(j + 1)
    elseif g_state == STATE_GOT_HEADER then
        response_body_encoded = payload
    else
        --print("ERROR: Invalid state when recieving from server. g_state = ", g_state)
        conn:close()
        return
    end
    
    -- Remove wrapping "..."
    start_index = 1
    end_index = -1
    if string.sub(response_body_encoded, start_index, start_index) == "\"" then
        start_index = start_index + 1 
    end
    if string.sub(response_body_encoded, end_index, end_index) == "\"" then
        end_index = end_index - 1
    end
    response_body_encoded = string.sub(response_body_encoded, start_index, end_index)
    
    file.write(response_body_encoded)
    file.flush()
   
    
    g_content_len = g_content_len - response_body_encoded:len()
    --print("INFO: Remaining g_content_len: ", g_content_len, " bytes.")
    if g_content_len == 0 then
        conn:close()
        file.close()
        g_state = STATE_GOT_PAYLOAD
        base64DecodeFile(BASE64_PAYLOAD_FILE_PATH, PAYLOAD_FILE_PATH)
		file_size = getFileSize(PAYLOAD_FILE_PATH)
        uart.write(0, numToBinaryString(file_size))
        g_state = STATE_GET_TOTAL_SIZE_RESPONDED
		--print("INFO: Finished getting payload! file size: ", file_size)
    end
end

function getSongContent()
    --print("TRACE: getSongContent is called.")
    g_state = STATE_RECEIVE_FROM_SERVER
    --tmr.alarm(GET_SONG_CONTENT_TIMER_ID, TIMER_TICK_INTERVAL_MS, tmr.ALARM_SINGLE, getSongContent)
    conn=net.createConnection(net.TCP, 0)
    conn:on("receive", onGetSongContentReceive)
    conn:on("connection", function(c)
           conn:send(
           "GET /api/SongContent HTTP/1.1\r\n"
           .."Host: bothovenmobileservice.azure-mobile.net\r\n"
           .."Connection: keep-alive\r\n"
           .."Authorization: Basic Qm90aG92ZW46b2hzcWNVZ29xeVNjVE1PWmRtWU9MWlZEdmVNakhONDI=\r\n"
           .."User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36\r\n"
           .."Accept: */*\r\n"
           .."Referer: http://bothovenmobileservice.azure-mobile.net/help/Api/GET-api-SongContent\r\n"
           .."\r\n")
           end)
    conn:connect(80, "bothovenmobileservice.azure-mobile.net")
end

function onWifiConnected()
    --uart.setup(0,9600,8,0,0)
    uart.on("data", 5, onUartReceive, 0)
end

function connectAP()
    wifi.setmode(wifi.STATION)
    wifi.sta.config(SSID_NAME,WIFI_PASS)
	--wifi.sta.eventMonReg(wifi.STA_GOTIP, onWifiConnected)
    --print("wifi.sta.status():", wifi.sta.status())
    --print("wifi.sta.getip():", wifi.sta.getip())
end

function setup()
    connectAP()
	tmr.alarm(0,2 * 1000, 0, onWifiConnected)
end

function main()
    --print("Hello, World!")
    setup()
	--getSongContent()
end
---------------------[[ Main ]]-------------------------------------
main()