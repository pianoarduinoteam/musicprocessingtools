import socket
import sys

song_id = "2"
if (len(sys.argv) > 1):
	song_id = sys.argv[1]

s = socket.socket()
s.connect(("bothovenmobileservice.azure-mobile.net", 80))
s.sendall("GET /api/SelectSong?songId=" + song_id + " HTTP/1.1\r\n"
           +"Host: bothovenmobileservice.azure-mobile.net\r\n"
           +"Connection: keep-alive\r\n"
           +"Authorization: Basic Qm90aG92ZW46b2hzcWNVZ29xeVNjVE1PWmRtWU9MWlZEdmVNakhONDI=\r\n"
           +"User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36\r\n"
           +"Accept: */*\r\n"
           +"Referer: http://bothovenmobileservice.azure-mobile.net/help/Api/GET-api-SongContent\r\n"
           +"\r\n")
print s.recv(1024 * 1024)
s.close()
print "RESET COMPLETED!"