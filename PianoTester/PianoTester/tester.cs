﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Toub.Sound.Midi;

namespace BotHoven
{
    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< start Dor Part
    class Settings
    {

        public int handsNum;
        public bool[][] fingerConf; /* true == white, false == black */
        public int stepsPerKey;
        public int[] keysTransitionMs;
        public int[] fingerDownMs;
        public int[] fingerUpMs;
        public int[] fingerNums;
        public bool[] isKeyWhiteBool;
        public int firstKey;
        public int lastKey;
        public int[] handsMotorIds;
        public int[] fingersMotorIds;
        public double noteTolerancePrecentage;

        string getKeyValue(string key)
        {
            return ConfigurationManager.AppSettings[key].Replace(" ", "");
        }

        int[] keyToArrInt(string key)
        {
            int[] intVals;
            string[] strVals = getKeyValue(key).Split(',');
            intVals = new int[strVals.Length];
            for (int i = 0; i < strVals.Length; i++)
            {
                intVals[i] = int.Parse(strVals[i]);
            }

            return intVals;
        }

        public bool isKeyWhite(int keyNum)
        {
            return isKeyWhiteBool[keyNum];
        }

        public Settings()
        {

            int[] blackKeysIdxs = {22, 25, 27, 30, 32, 34, 37, 39,
                                   42, 44, 46, 49, 51, 54, 56, 58,
                                   61, 63, 66, 68, 70, 73, 75, 78,
                                   80, 82, 85, 87, 90, 92, 94, 97,
                                   99, 102, 104, 106};
            isKeyWhiteBool = Enumerable.Repeat(true, 150).ToArray();
            foreach (int key in blackKeysIdxs)
            {
                isKeyWhiteBool[key] = false;
            }

            handsNum = int.Parse(getKeyValue("handsNum"));
            fingerConf = new bool[handsNum][];
            fingerNums = new int[handsNum];
            int i = 0;

            foreach (string handConf in getKeyValue("fingerConf").Split(','))
            {
                fingerConf[i] = new bool[handConf.Length];
                fingerNums[i] = 0;
                for (int j = 0; j < handConf.Length; j++)
                {
                    fingerConf[i][j] = (handConf[j] == '0');
                    fingerNums[i]++;
                }
                ++i;
            }

            stepsPerKey = int.Parse(getKeyValue("stepsPerKey"));

            keysTransitionMs = keyToArrInt("keysTransitionMs");
            fingerDownMs = keyToArrInt("fingerDownMs");
            fingerUpMs = keyToArrInt("fingerUpMs");
            firstKey = int.Parse(getKeyValue("firstKey"));
            lastKey = int.Parse(getKeyValue("lastKey"));
            handsMotorIds = keyToArrInt("handsMotorIds");
            fingersMotorIds = keyToArrInt("fingersMotorIds");
            noteTolerancePrecentage = (100 - Double.Parse(getKeyValue("noteTolerancePrecentage"))) / 100;

        }

    }
    /* struct ArduinoInputFormat
     {
         public int startTimeMillis;
         public int motorId;
         public int steps;
         public int velocity;
     }*/
    struct ArduinoInputFormat
    {
        public int startTimeMillis;
        public byte motorId;
        public sbyte steps;
        public byte velocity;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> end Dor Part
    public class Piano
    {
        public Piano()
        {
            MidiPlayer.OpenMidi();
        }
        ~Piano()
        {
            MidiPlayer.CloseMidi();
        }
        public void downNote(int Note, int velocity)
        {
            Console.WriteLine("(N) Number: . . . . . . . . . {0:N}\n", Note);
            NoteOn note = new NoteOn(0, 1, (Byte)(Note), (Byte)velocity);
            if (note != null) MidiPlayer.Play(note);
        }
        public void upNote(int Note, int velocity)
        {
            NoteOff note = new NoteOff(0, 1, (Byte)(Note), (Byte)velocity);
            if (note != null) MidiPlayer.Play(note);
        }
    }
    public class Hand
    {
        static Settings set = new Settings();
        Finger[] fingers;
        Piano piano;
        int[] delayPerStep;
        int place;
        List<int> pressedKeys = new List<int>();
        public Hand(Piano piano, int[] delayFingerUp, int[] delayFingerDown, int[] moveDelay)
        {
            this.piano = piano;
            this.fingers = new Finger[delayFingerDown.Length];
            this.delayPerStep = new int[moveDelay.Length];
            moveDelay.CopyTo(this.delayPerStep, 0);
            this.place = (int)(set.firstKey);
            for (int i = 0; i < delayFingerDown.Length; i++)
            {
                fingers[i] = new Finger();
                fingers[i].setDelayUp(delayFingerUp[i]);
                fingers[i].setDelayDown(delayFingerDown[i]);
            }
        }
        public void move(int steps)
        {
            if ((this.place < 0) || (this.place > 127)) throw new SystemException(String.Concat("out of bounds of the organ! place:",this.place.ToString()));
            if (pressedKeys.Count != 0) throw new SystemException(String.Concat("don't try move while key is pressed! num of keys down: ",pressedKeys.Count.ToString()));
            System.Threading.Thread.Sleep(this.delayPerStep[System.Math.Abs(steps) / set.stepsPerKey]);

            /* Dor's part */
            int keys = steps / set.stepsPerKey;
            int delta = 1;
            if (keys < 0)
            {
                delta = -1;
            }

            while (Math.Abs(keys) > 0)
            {
                place += delta;
                if (set.isKeyWhite(place))
                {
                    keys -= delta;
                }

            }
        }
        static int getLeftFingerKey(int key, int finger)
        {
            int pos = key;
            for (int i = 0; i < finger; i++)
            {
                if (set.isKeyWhite(pos) == set.fingerConf[0][i])
                {
                    pos++;
                }
            }

            return pos;
        }
        public void downFinger(int fingerNum, int velocity)
        {
            if (pressedKeys.Contains(fingerNum)) throw new SystemException(String.Concat("finger is already down! finger num: ",fingerNum.ToString()));
            pressedKeys.Add(fingerNum);
            fingers[fingerNum].down(this.piano, getLeftFingerKey(this.place, fingerNum), velocity);
        }
        public void upFinger(int fingerNum, int velocity)
        {
            if (!pressedKeys.Contains(fingerNum)) throw new SystemException(String.Concat("finger is already up! finger num: ", fingerNum.ToString()));
            pressedKeys.Remove(fingerNum);
            fingers[fingerNum].up(this.piano, getLeftFingerKey(this.place, fingerNum), velocity);
        }
    }
    public class Finger
    {
        int delayDown;
        int delayUp;
        public Finger()
        {
            this.delayDown = 0;
            this.delayUp = 0;
        }
        public void setDelayDown(int delay)
        {
            this.delayDown = delay;
        }
        public void setDelayUp(int delay)
        {
            this.delayUp = delay;
        }
        public void down(Piano piano, int tileNum, int velocity)
        {
            System.Threading.Thread.Sleep(delayDown);
            piano.downNote(tileNum, velocity);
        }
        public void up(Piano piano, int tileNum, int velocity)
        {
            System.Threading.Thread.Sleep(delayUp);
            piano.upNote(tileNum, velocity);
        }

    }
    class Program
    {
        static Settings set = new Settings();
        static List<ArduinoInputFormat> ardCommands = new List<ArduinoInputFormat>();
        enum CmdLineArgs
        {
            MidiFilePath = 0,
            // Must be last
            Count
        };
        static void readResults(string inputFileName)
        {
            string inFileName = inputFileName;
            System.IO.BinaryReader binStream = new System.IO.BinaryReader(System.IO.File.Open(inFileName, System.IO.FileMode.Open));

            while (binStream.BaseStream.Position < binStream.BaseStream.Length)
            {
                ArduinoInputFormat readInput;
                readInput.startTimeMillis = binStream.ReadInt32();
                readInput.motorId = binStream.ReadByte();
                readInput.steps = binStream.ReadSByte();
                readInput.velocity = binStream.ReadByte();
                ardCommands.Add(readInput);
            }
            binStream.Close();
        }
        static void runTester()
        {
            Hand[] hands = new Hand[set.handsMotorIds.Length];
            for (int i = 0; i < set.handsMotorIds.Length; i++)
                hands[i] = new Hand(new Piano(), set.fingerUpMs, set.fingerDownMs, set.keysTransitionMs);
            int prevTime = 0, curTime = 0;
            foreach (ArduinoInputFormat Commands in ardCommands)
            {
                if (Commands.startTimeMillis != 0)
                {
                    curTime = Commands.startTimeMillis;
                    System.Threading.Thread.Sleep(System.Math.Abs(curTime - prevTime));
                }
                if (Commands.motorId < set.handsMotorIds.Length)
                {
                    hands[Commands.motorId].move(Commands.steps);
                }
                else
                {
                    if (Commands.steps > 0)
                    {
                        hands[0].downFinger(Commands.motorId - set.handsMotorIds.Length, Commands.velocity);
                    }
                    else
                    {
                        hands[0].upFinger(Commands.motorId - set.handsMotorIds.Length, Commands.velocity);
                    }
                }
                if (Commands.startTimeMillis != 0)
                {
                    prevTime = Commands.startTimeMillis;
                }
            }
        }

        static void Main(string[] args)
        {
            if (args.Length < (int)CmdLineArgs.Count)
            {
                Console.Out.WriteLine("Invalid usage. Expected: BotHoven.exe <midi_parsed_file>");
                return;
            }
            string file = args[(int)CmdLineArgs.MidiFilePath];
            readResults(file);
            try
            {
                runTester();
            }
            catch (SystemException e)
            {
                Console.WriteLine(e.Message);
                System.Threading.Thread.Sleep(3000);
            }
        }
    }
}
