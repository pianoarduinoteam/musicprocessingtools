﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.WindowsAzure.MobileServices;
using System.Net.Http;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MobileApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public List<int> songsList;
        private static System.Threading.SemaphoreSlim sem = new System.Threading.SemaphoreSlim(1, 1);

        public MainPage()
        {
            this.InitializeComponent();
            updateList();
            
            songsList = new List<int>();
            Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                     (() => tryChangeSong()));
            
            Story.Begin();


        }


        public async void tryChangeSong()
        {

            while (true)
            {

                sem.Wait();

                if (songsList.Count != 0)
                {

                    int songId = -1;

                    await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                        (() => songId = (int)((ListViewItem)listView.Items[songsList[0]]).Tag));

                    try
                    {
                        await App.MobileService.InvokeApiAsync(
                            "SelectSong",
                            HttpMethod.Get,
                            new Dictionary<string, string>() { { "songId", songId.ToString() } });

                        await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                            (() => NowPlaying.Text = ((ListViewItem)listView.Items[songsList[0]]).Content.ToString()));

                        Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                                            (() => listView.DeselectRange(new ItemIndexRange(songsList[0], 1))));
                    }
                    catch (Microsoft.WindowsAzure.MobileServices.MobileServiceInvalidOperationException e)
                    {

                    }
                }

                sem.Release(1);

                string songName = await App.MobileService.InvokeApiAsync<string>(
                            "CurSong",
                            HttpMethod.Get,
                            null);
                /*await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                                          (() => NowPlaying.Text = songName));*/

                await System.Threading.Tasks.Task.Delay(10000);

            }
        }

        public async void updateList()
        {
            Dictionary<string, string> songs =
            await App.MobileService.InvokeApiAsync<Dictionary<string, string>>(
                "SongList",
                HttpMethod.Get,
                null);

            foreach (KeyValuePair<string, string> pair in songs)
            {
                ListViewItem item = new ListViewItem();
                item.Tag = int.Parse(pair.Key);
                item.Content = pair.Value;
                listView.Items.Add(item);
                SongIndices.Items.Add(new ListViewItem());
            }
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            sem.Wait();

            foreach (ListViewItem item in e.RemovedItems)
            {
                int idx = listView.Items.IndexOf(item);
                songsList.Remove(idx);
                if (idx < 0)
                {
                    continue;
                }
                ((ListViewItem)SongIndices.Items[idx]).Content = "";
                SongIndices.DeselectRange(new ItemIndexRange(idx, 1));
            }

            foreach (ListViewItem item in e.AddedItems)
            {
                int idx = listView.Items.IndexOf(item);
                if (idx < 0)
                {
                    continue;
                }
                songsList.Add(idx);
                SongIndices.SelectRange(new ItemIndexRange(idx, 1));
            }

            int pos = 1;
            foreach (int item in songsList)
            {
                ((ListViewItem)SongIndices.Items[item]).Content = pos.ToString();
                pos++;
            }

            sem.Release(1);
        }


    }
}
