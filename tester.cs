﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Toub.Sound.Midi;
namespace BotHoven
{
    public class Piano
    {
        string[] notes;
        public Piano(int size)
        {
            notes = new string[size];
            MidiPlayer.OpenMidi();
            for (int i = 0; i < size/12;i++)
            {
                notes[0 + i * 12] = "C" + (i+2).ToString();
                notes[1 + i * 12] = "C#" + (i + 2).ToString();
                notes[2 + i * 12] = "D" + (i + 2).ToString();
                notes[3 + i * 12] = "D#" + (i + 2).ToString();
                notes[4 + i * 12] = "E" + (i +2).ToString();
                notes[5 + i * 12] = "F" + (i +2).ToString();
                notes[6 + i * 12] = "F#" + (i +2).ToString();
                notes[7 + i * 12] = "G" + (i +2).ToString();
                notes[8 + i * 12] = "G#" + (i +2).ToString();
                notes[9 + i * 12] = "A" + (i +2).ToString();
                notes[10 + i * 12] = "A#" + (i +2).ToString();
                notes[11 + i * 12] = "B" + (i +2).ToString();
            }
           
        }
        ~Piano()
        {
           
        }
        public void downNote(int Note)
        {
           // this.Num2Note(Note)
            Console.WriteLine("(N) Number: . . . . . . . . . {0:N}\n", Note);
            NoteOn note = new NoteOn(0, 1,(Byte)(Note) , 127);
            if(note != null) MidiPlayer.Play(note);
        }
        public void upNote(int Note)
        {
            NoteOff note = new NoteOff(0, 1, (Byte)(Note), 127);
            if (note != null) MidiPlayer.Play(note);
        }
        public string Num2Note(int num){
            return notes[num/100];
        }
        public int getSize()
        {
            return this.notes.Length;
        }
    }
    public class Hand
    {
        static Settings set = new Settings();
        Finger[] fingers;
        Piano piano;
        int[] delayPerStep;
        int place;
       public Hand(Piano piano,int[] delayFingerUp,int[] delayFingerDown, int[] moveDelay){
            this.piano = piano;
            this.fingers = new Finger[delayFingerDown.Length];
            this.delayPerStep = new int[moveDelay.Length];
            moveDelay.CopyTo(this.delayPerStep,0);
            this.place = (int) (set.firstKey);
            for (int i = 0; i < delayFingerDown.Length; i++)
            {
                fingers[i] = new Finger();
                fingers[i].setDelayUp(delayFingerUp[i]);
                fingers[i].setDelayDown(delayFingerDown[i]);
            }
        }
        public void move(int steps)
        {
            //if(((this.place+steps-fingers.Length/2)<0) || ((this.place+steps+fingers.Length/2)>piano.getSize())) throw new SystemException("out of bounds");
            
            System.Threading.Thread.Sleep(this.delayPerStep[System.Math.Abs(steps) / set.stepsPerKey]);

            /* Dor's part */
            int keys = steps / set.stepsPerKey;
            int delta = 1;
            if (keys < 0) {
                delta = -1;
            }

            while (Math.Abs(keys) > 0)
            {
                place += delta;
                if (set.isKeyWhite(place))
                {
                    keys -= delta;
                }
                
            }
        }
        static int getLeftFingerKey(int key, int finger)
        {
            int pos = key;
            for (int i = 0; i<finger; i++)
            {
                if (set.isKeyWhite(pos) == set.fingerConf[0][i])
                {
                    pos++;
                }
            }

            return pos;
        }
        public void downFinger(int fingerNum)
        {
            fingers[fingerNum].down(this.piano, getLeftFingerKey(this.place, fingerNum));
        }
        public void upFinger(int fingerNum)
        {
            fingers[fingerNum].up(this.piano, getLeftFingerKey(this.place, fingerNum));
        }
    }
    public class Finger
    {
        int delayDown;
        int delayUp;
        public Finger()
        {
            this.delayDown = 0;
            this.delayUp = 0;
        }
        public void setDelayDown(int delay)
        {
            this.delayDown = delay;
        }
        public void setDelayUp(int delay)
        {
            this.delayUp = delay;
        }
        public void down(Piano piano,int tileNum){
            System.Threading.Thread.Sleep(delayDown);
            piano.downNote(tileNum);
        }
        public void up(Piano piano, int tileNum)
        {
            System.Threading.Thread.Sleep(delayUp);
            piano.upNote(tileNum);
        }
    }
  /*  class Program
    {
        static void Main(string[] args)
        {
           Piano piano=new Piano(60);
           List<string[]> list = FileParse("C:\\Users\\Assaf Benarosh\\Documents\\Visual Studio 2013\\Projects\\piano\\piano\\test.txt");
           for (int i = 0; i < list.Count; i++) Console.WriteLine(list[i][0] + "," + list[i][1] + "," + list[i][2]);
           piano.downNote(48);
           System.Threading.Thread.Sleep(1000);
           piano.upNote(48);
           Console.ReadLine();
        }

        static List<string[]> FileParse(string filename)
        {
            List<string[]> list = new List<string[]>();
            string line;
            System.IO.StreamReader file =
               new System.IO.StreamReader(filename);
            while ((line = file.ReadLine()) != null)
            {
                list.Add(line.Split(','));
            }
            file.Close();
            return list;
        }
    }*/
}
