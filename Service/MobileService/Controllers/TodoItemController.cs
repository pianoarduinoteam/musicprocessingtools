﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using MobileService.DataObjects;
using MobileService.Models;

namespace MobileService.Controllers
{
    public class AddNumbersController : ApiController
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
        }

        public int GetAddNums(int[] arr)
        {
            return arr[0] + arr[1];
        }
        
    }
}