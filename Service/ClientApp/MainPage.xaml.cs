﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.WindowsAzure.MobileServices;
using System.Net.Http;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ClientApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        
       

        public MainPage()
        {
            this.InitializeComponent();
        
        }

        async private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            int res = await App.MobileService.InvokeApiAsync<int, int>(
                "AddNumbers",
                0,
                HttpMethod.Get,
                new Dictionary<string, string> { { "x", Num1.Text }, { "y", Num2.Text } });

            Result.Text = res.ToString();

        }
    }
}
