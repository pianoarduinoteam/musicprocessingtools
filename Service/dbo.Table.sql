﻿CREATE TABLE [dbo].[Table]
(
	[song_id] INT NOT NULL PRIMARY KEY, 
    [song_name] NCHAR(10) NULL, 
    [Content] VARBINARY(MAX) NULL
)
