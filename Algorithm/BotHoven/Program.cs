﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BotHoven
{

    struct ArduinoInputFormat {
        public int startTimeMillis;
        public byte motorId;
        public sbyte keys;
        public byte velocity;
    }

    class Settings
    {

        // Number of available hands
        public int handsNum;
        // For each hand, how are the fingers aligned (true == white, false == black)
        public bool[][] fingerConf; 
        // How many steps are needed to move a key
        public int stepsPerKey;
        // Transition time for X keys (left/right) in ms
        public int[] keysTransitionMs;
        // Transition time for finger down in ms
        public int[][] fingerDownMs;
        // Transition time for finger up in ms
        public int[][] fingerUpMs;
        // Number of fingers for each hand
        public int[] fingerNums;
        // A map of the black/white notes. keyNum -> isWhite{true/false}
        public bool[] isKeyWhiteBool;
        // The left-most key available in the piano
        public int firstKey;
        // The right-most key available in the piano
        public int lastKey;
        // A map of hand->Arduino motorId
        public int[] handsMotorIds;
        // A map of finger->Arduino motorId
        public int[] fingersMotorIds;
        // The minimum time to play last note
        public int noteMinPlayTime;

        string getKeyValue(string key)
        {
            return ConfigurationManager.AppSettings[key].Replace(" ", "");
        }

        int[][] keyToArrInt(string key)
        {
            int[][] intVals;
            string[] strVals = getKeyValue(key).Split(';');
            intVals = new int[strVals.Length][];
            for (int i = 0; i < strVals.Length; i++)
            {
                string[] inStrVals = strVals[i].Split(',');
                intVals[i] = new int[inStrVals.Length];
                for (int j = 0; j < inStrVals.Length; j++)
                    intVals[i][j] = int.Parse(inStrVals[j]);
            }

            return intVals;
        }

        public bool isKeyWhite(int keyNum)
        {
            return isKeyWhiteBool[keyNum];
        }

        public bool isHandMotor(int motorId)
        {
            return Array.IndexOf(handsMotorIds, motorId) != -1;
        }

        public Settings()
        {
            
            int[] blackKeysIdxs = {22, 25, 27, 30, 32, 34, 37, 39,
                                   42, 44, 46, 49, 51, 54, 56, 58,
                                   61, 63, 66, 68, 70, 73, 75, 78,
                                   80, 82, 85, 87, 90, 92, 94, 97,
                                   99, 102, 104, 106};
            isKeyWhiteBool = Enumerable.Repeat(true, 150).ToArray();
            foreach (int key in blackKeysIdxs) {
                isKeyWhiteBool[key] = false;
            }

            handsNum = int.Parse(getKeyValue("handsNum"));
            fingerConf = new bool[handsNum][];
            fingerNums = new int[handsNum];
            int i = 0;

            foreach (string handConf in getKeyValue("fingerConf").Split(','))
            {
                fingerConf[i] = new bool[handConf.Length];
                fingerNums[i] = 0;
                for (int j = 0; j < handConf.Length; j++)
                {
                    fingerConf[i][j] = (handConf[j] == '0');
                    fingerNums[i]++;
                }
                ++i;
            }

            stepsPerKey = int.Parse(getKeyValue("stepsPerKey"));

            keysTransitionMs = keyToArrInt("keysTransitionMs")[0];
            fingerDownMs = keyToArrInt("fingerDownMs");
            fingerUpMs = keyToArrInt("fingerUpMs");
            firstKey = int.Parse(getKeyValue("firstKey"));
            lastKey = int.Parse(getKeyValue("lastKey"));
            handsMotorIds = keyToArrInt("handsMotorIds")[0];
            fingersMotorIds = keyToArrInt("fingersMotorIds")[0];
            noteMinPlayTime = int.Parse(getKeyValue("noteMinPlayTime"));
        }

    }


    class Note
    {
        public int key;
        public int startTime;
        public int duration;
        public int velocity;
        public int hand;
        public int finger;

        public Note (int key, int startTime, int duration, int velocity)
        {
            this.key = key;
            this.startTime = startTime;
            this.duration = duration;
            this.velocity = velocity;
        }

    }

    class Program
    {

        static Settings set = new Settings();
        static List<Note> inputNotes_G = new List<Note>();
        static List<ArduinoInputFormat> ardCommands = new List<ArduinoInputFormat>();
        static int notesNum = 0;


        enum CmdLineArgs
        {
            MidiFilePath = 0,
            // Must be last
            Count
        };

        /*
        function readInputFile
            Description: Reads the input file, and create a Note instance
                         for each line
            Input:  string filename - the file's name
            Output: void
        */
        static void readInputFile(string filename)
        {
            string line;
            string[] args;

            System.IO.StreamReader file =
                new System.IO.StreamReader(filename);

            while ((line = file.ReadLine()) != null)
            {
                args = line.Split(',');
                inputNotes_G.Add(new Note(int.Parse(args[0]),
                                          int.Parse(args[1]),
                                          int.Parse(args[2]),
                                          int.Parse(args[3])
                                         )
                                );
                ++notesNum;
            }

            file.Close();
        }

        /*
        function getLeftFingerKey
            Description: Calculates the position of the left-most finger of the hand
                         from a finger and the key it is playing
            Input:  int hand    - Hand number
                    int key     - The key number the finger is on
                    int finger  - The finger that plays the key
            Output: int         - The hand's left-most finger position (the #key under it)
        */
        static int getLeftFingerKey(int hand, int key, int finger)
        {
            int pos = key;

            while (finger > 0)
            {
                pos--;
                if (set.isKeyWhite(pos))
                {
                    finger--;
                }
            }

            return pos;
        }

        /*
        function getDistInKeys
            Description: Calculates the distance in keys from a hand playing srcKey with  
                         srcFinger to the hand playing dstKey with dstFinger
            Input:  int hand    - Hand number
                    int srcKey  - The first key the hand plays
                    int srcFin  - The finger that plays the first key
                    int dstKey  - The second key the hand plays
                    int dstFin  - The finger that plays the second key
            Output: int         - The distance in keys needed for the 
                                  input configuration (Can be negative)
        */
        static int getDistInKeys(int hand, int srcKey, int srcFin, int dstKey, int dstFin)
        {
            int leftFingerSrc = getLeftFingerKey(hand, srcKey, srcFin);
            int leftFingerDest = getLeftFingerKey(hand, dstKey, dstFin);
            int dist = 0;

            for (int i = Math.Min(leftFingerSrc,leftFingerDest);
                 i < Math.Max(leftFingerSrc, leftFingerDest);
                 i++)
            {
                if (set.isKeyWhite(i))
                {
                    dist++;
                }
            }

            if (leftFingerSrc > leftFingerDest) {
                dist *= -1;
            }

            return dist;
        }

        /*
        function getTransitionTimeMs
            Description: Calculates the transition time needed (in ms) for playing
                         dstKey with dstFin after srcKey was played with srcFin
            Input:  int hand    - Hand number
                    int srcKey  - The first key the hand plays
                    int srcFin  - The finger that plays the first key
                    int dstKey  - The second key the hand plays
                    int dstFin  - The finger that plays the second key
            Output: int         - The transition time in ms
        */
        static int getTransitionTimeMs(int hand, int srcKey, int srcFin, int dstKey, int dstFin)
        {
            int dist = getDistInKeys(hand, srcKey, srcFin, dstKey, dstFin);
            return set.fingerUpMs[hand][srcFin] + 
                   set.keysTransitionMs[dist] + 
                   set.fingerDownMs[hand][dstFin];
        }

        /*
        function getSpeedUpFactor
            Description: Calculates the max speed in which the song can be played 
                         when choosing dstFin to play note[keyIndex] after srcFin was choosen
            Input:  int keyIndex - The note that needs to be played
                    int srcFin   - The finger that plays the first key
                    int dstFin   - The finger that plays the second key
            Output: int          - The max factor that the song times can be divided by
        */
        static double getSpeedUpFactor(int keyIndex, int hand, int srcFin, int dstFin)
        {
            int origDuration = inputNotes_G[keyIndex].duration;
            int dist = Math.Abs(getDistInKeys(0, inputNotes_G[keyIndex - 1].key, srcFin, 
                                              inputNotes_G[keyIndex].key, dstFin));
            if (dist == 0 && srcFin != dstFin)
            {
                return 10000;
            }

            int feasableTime = set.fingerUpMs[hand][srcFin] + 
                               set.keysTransitionMs[dist] + 
                               set.fingerDownMs[hand][dstFin];


            return (double)(inputNotes_G[keyIndex].startTime - inputNotes_G[keyIndex - 1].startTime) 
                   / (double)(feasableTime + set.noteMinPlayTime);
        }

        /*
        function calcMoveTimes
            Description: The main algorithm. Prepares an appropriate table
                         and applies dynammic programming, to calculate the
                         optimal hands' alignments and transitions while considering
                         each hand and finger's limitations.
            Input:  
            Output: double    - The max factor that the song times can be divided by
                                (Cannot be more than 1 - The original speed)
        */
        static double calcMoveTimes()
        {
            double[][] dynamicFactorsTable = new double[notesNum][];
            int[][] dynamicTableChoice = new int[notesNum][];

            dynamicFactorsTable[0] = new double[set.fingerNums[0]];
            dynamicTableChoice[0] = new int[set.fingerNums[0]];

            /* Initialize the first row */
            for (int fin = 0; fin < set.fingerNums[0]; fin++)
            {
                dynamicFactorsTable[0][fin] = 10000;
                dynamicTableChoice[0][fin] = 0;
            }

            /* Use the DP function on each cell of the table */
            for (int note = 1; note < notesNum; note++)
            {
                dynamicFactorsTable[note] = new double[set.fingerNums[0]];
                dynamicTableChoice[note] = new int[set.fingerNums[0]];
                for (int fin = 0; fin < set.fingerNums[0]; fin++)
                {
                    /* If this finger cannot play this note (different colors) */
                    if (set.isKeyWhiteBool[inputNotes_G[note].key] != set.fingerConf[0][fin])
                    {
                        dynamicFactorsTable[note][fin] = -1;
                        dynamicTableChoice[note][fin] = -1;
                        continue;
                    }

                    dynamicFactorsTable[note][fin] = -1;
                    for (int srcFin = 0; srcFin < set.fingerNums[0]; srcFin++)
                    {
                        if (getSpeedUpFactor(note, 0, srcFin, fin) > dynamicFactorsTable[note][fin] &&
                            dynamicTableChoice[note - 1][srcFin] != -1)
                        {
                            dynamicFactorsTable[note][fin] = getSpeedUpFactor(note, 0, srcFin, fin);
                                                                      //dynamicFactorsTable[note - 1][srcFin]);
                            dynamicTableChoice[note][fin] = srcFin;
                        }
                    }

                    dynamicFactorsTable[note][fin] = Math.Min(dynamicFactorsTable[note][fin],
                                                              dynamicFactorsTable[note - 1][dynamicTableChoice[note][fin]]);

                    if (dynamicFactorsTable[note][fin] == -1)
                    {
                        throw new Exception("Cannot play the song with current hand configuration");
                    }
                }
            }

            double maxSpeedUp = 0;

            inputNotes_G[notesNum - 1].finger = 0;

            /* Choose the best path in the table to start from */
            for (int fin = 0; fin < set.fingerNums[0]; fin++)
            {
                if (dynamicFactorsTable[notesNum - 1][fin] > maxSpeedUp)
                {
                    maxSpeedUp = dynamicFactorsTable[notesNum - 1][fin];
                    inputNotes_G[notesNum - 1].finger = fin;
                    inputNotes_G[notesNum - 1].hand = 0;
                }
            }

            /* Go back in the choosen path and collect the choosen hands & fingers */
            for (int note = notesNum - 2; note >= 0; note--)
            {
                inputNotes_G[note].finger = dynamicTableChoice[note + 1][inputNotes_G[note + 1].finger];
                inputNotes_G[note].hand = 0;
            }

            /* To be on the safe side (Even though it was already limited) */
            return Math.Min(maxSpeedUp, 1);

        }

        /*
        function applySpeedUpFactor
            Description: Applies the input speedup factor on the whole song
            Input:  double    - The factor the song will be divided by
            Output: void
        */
        static void applySpeedUpFactor(double speedUpFactor)
        {
            for (int i = 0; i < notesNum; i++)
            {
                inputNotes_G[i].startTime = (int)(inputNotes_G[i].startTime / speedUpFactor);
                inputNotes_G[i].duration = (int)(inputNotes_G[i].duration / speedUpFactor);
            }
        }

        /*
        function fillArduinoCommands
            Description: For each note in the song, prepares appropriate
                         hand & finger movement commands (Used by Arduino & Tester)
            Input: 
            Output: void
        */
        static void fillArduinoCommands()
        {

            ArduinoInputFormat com;
            int dist;
            int offset = 0;

            for (int i = 0; i < notesNum; i++)
            {
                /* Hand movement command */
                if (i == 0)
                {
                    dist = getDistInKeys(0, set.firstKey, 0,
                                         inputNotes_G[i].key, inputNotes_G[i].finger);
                }
                else
                {
                    dist = getDistInKeys(0, inputNotes_G[i - 1].key, inputNotes_G[i - 1].finger,
                                         inputNotes_G[i].key, inputNotes_G[i].finger);
                }

                if (dist != 0) {
                    com.keys = (sbyte)dist;
                    if (i == 0)
                    {
                        com.startTimeMillis = 0;
                        offset = set.keysTransitionMs[Math.Abs(dist)] +
                                 set.fingerDownMs[inputNotes_G[0].hand][inputNotes_G[0].finger];

                    } else
                    {
                        com.startTimeMillis = inputNotes_G[i - 1].startTime +
                                              set.noteMinPlayTime +
                                              set.fingerUpMs[inputNotes_G[i - 1].hand][inputNotes_G[i - 1].finger] + 
                                              offset;
                    }

                    com.motorId = (byte)set.handsMotorIds[inputNotes_G[i].hand];
                    com.velocity = 0;
                    ardCommands.Add(com);
                }
                /* Finger down command */
                com.keys = 1;
                com.startTimeMillis = inputNotes_G[i].startTime -
                                      set.fingerDownMs[inputNotes_G[i].hand][inputNotes_G[i].finger] +
                                      offset;
                com.motorId = (byte)set.fingersMotorIds[inputNotes_G[i].finger];
                com.velocity = (byte)inputNotes_G[i].velocity;
                ardCommands.Add(com);

                /* Finger up command */
                com.keys = -1;
                com.startTimeMillis = inputNotes_G[i].startTime + set.noteMinPlayTime + offset;

                /* Check if finger needs to lift earlier */
                /*if (i < notesNum - 1)
                {
                    dist = getDistInKeys(0, inputNotes_G[i].key, inputNotes_G[i].finger,
                                         inputNotes_G[i + 1].key, inputNotes_G[i + 1].finger);

                    com.startTimeMillis = Math.Min(com.startTimeMillis,
                                                   inputNotes_G[i + 1].startTime -
                                                   set.fingerUpMs[inputNotes_G[i].hand][inputNotes_G[i].finger] - 
                                                   set.keysTransitionMs[Math.Abs(dist)] - 
                                                   set.fingerDownMs[inputNotes_G[i+1].hand][inputNotes_G[i+1].finger] +
                                                   offset);
                }*/

                com.motorId = (byte)set.fingersMotorIds[inputNotes_G[i].finger];
                com.velocity = 0;
                ardCommands.Add(com);

            }

            ardCommands.Sort(
                delegate (ArduinoInputFormat a, ArduinoInputFormat b) {
                    return a.startTimeMillis.CompareTo(b.startTimeMillis);
                }
            );

            com.startTimeMillis = ardCommands[ardCommands.Count - 1].startTimeMillis + 5000;
            com.motorId = 0;
            com.keys = (sbyte)(getDistInKeys(0, inputNotes_G[inputNotes_G.Count - 1].key, inputNotes_G[inputNotes_G.Count - 1].finger, set.firstKey, inputNotes_G[inputNotes_G.Count - 1].finger));
            com.velocity = 0;

            ardCommands.Add(com);

        }

        /*
        function checkResults
            Description: Validates the commands. If a move command is executed while
                         a finger is down, or 2 down/up commands are executed in a row,
                         throws exception
            Input: void
            Output: void
        */
        static void checkResults()
        {

            bool[] fingerArray = new bool[set.fingerNums[0]];

            int fingerCount = 0;

            for (int i = 0; i < fingerArray.Length; i++)
            {
                // Fingers are up
                fingerArray[i] = false;
            }

            for (int i = 0; i < ardCommands.Count; i++)
            {
                if(set.handsMotorIds.Contains(ardCommands[i].motorId))
                {
                    if (fingerCount > 0)
                    {
                        throw new Exception("Hand was moved while a finger is down");
                    }
                } else
                {
                    int pos;
                    for (pos = 0; pos < set.fingersMotorIds.Length; pos++)
                    {
                        if (set.fingersMotorIds[pos] == ardCommands[i].motorId)
                        {
                            break;
                        }
                    }
                    
                    if (pos == set.fingersMotorIds.Length)
                    {
                        throw new Exception("Unknown motor ID");
                    }

                    if (fingerArray[pos] && (ardCommands[i].keys == 1) ||
                        !fingerArray[pos] && (ardCommands[i].keys == -1))
                    {
                        throw new Exception("Finger cannot lift or drop twice");
                    }

                    // up will decrease the count, down will increase it
                    fingerCount += ardCommands[i].keys;

                    fingerArray[pos] = ardCommands[i].keys == 1;

                }

            }



        }

        /*
        function printResults
            Description: Prints log & command files based on fillArduinoCommands commands
            Input: string inputFileName - Name of the input filename for the output filenames
            Output: void
        */
        static void printResults(string inputFileName)
        {
            string outFileName = inputFileName + ".alg";
            string outLogName = inputFileName + ".log";

            System.IO.FileStream outFile =
                new System.IO.FileStream(outFileName, System.IO.FileMode.Create);
            System.IO.StreamWriter logFile = new System.IO.StreamWriter(outLogName, false);

            System.IO.BinaryWriter binStream = new System.IO.BinaryWriter(outFile);

            foreach (ArduinoInputFormat line in ardCommands)
            {
                binStream.Write(line.startTimeMillis);
                binStream.Write(line.motorId);
                binStream.Write(line.keys);
                binStream.Write(line.velocity);
                logFile.WriteLine("{0:D} {1:D} {2:D} {3:D}", 
                    line.startTimeMillis, line.motorId, line.keys, line.velocity);

            }

            binStream.Close();
            outFile.Close();
            logFile.Close();
        }

        static void Main(string[] args)
        {
            if (args.Length < (int)CmdLineArgs.Count)
            {
                Console.Out.WriteLine("Invalid usage. Expected: BotHoven.exe <midi_parsed_file>");
                return;
            }
            string file = args[(int)CmdLineArgs.MidiFilePath];

            readInputFile(file);

            double speedUpFactor = calcMoveTimes();
            Console.Out.WriteLine("A speedup of {0:F} was acheived", speedUpFactor);
            applySpeedUpFactor(speedUpFactor);
            fillArduinoCommands();
            checkResults();
            printResults(file);

            System.Threading.Thread.Sleep(2000);

        }
    }
}
